GraphQL Relaciones | Ejemplo Práctico

https://www.youtube.com/watch?v=M_FdNXBSDKI

{
  books{
    id
    title
    author{
      name
    }
    reviews{
      text
    }
  }
}

# GraphQL with graphql-yoga
![](screenshot.png)